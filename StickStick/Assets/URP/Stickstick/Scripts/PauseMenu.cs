﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement; 

public class PauseMenu : MonoBehaviour
{
    public void SaveMyGame () {
        
    }
    public void ContinueGame (){
        
    }
    public void ToMainMenu (){
        SceneManager.LoadScene ("Start"); 
    }
    public void QuitGame (){
        Application.Quit (); 
    }
}
