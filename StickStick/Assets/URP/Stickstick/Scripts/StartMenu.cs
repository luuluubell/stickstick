﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; 
using UnityEngine.SceneManagement;

public class StartMenu : MonoBehaviour {

    public void LoadGame () {
        
    }
    public void PlayGame () {
        SceneManager.LoadScene("Level"); 
    }
    public void CharacterSelect (){
        SceneManager.LoadScene("CharacterSelect"); 
    }
    public void QuitGame (){
        Application.Quit(); 
    }
}
