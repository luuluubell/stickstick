﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class BossTerritory : MonoBehaviour {
    public GameObject player; 
    public GameObject bossman; 
    public BoxCollider Bossterritory; //the collider for the area that the boss is in- must be different to other colliders 
    public bool playertouchyterritory; 
    BossScript boss; 

    void Start() {
        player = GameObject.FindGameObjectWithTag ("Player"); 
        boss = bossman.GetComponent <BossScript> ();
        playertouchyterritory = false;  
    }

    void Update() {
        if (playertouchyterritory == true) {
            boss.MoveToPlayer ();
        }
        if (playertouchyterritory == false) {
           boss.speed = 0; // boss.Rest (); 
        }
    }
    private void OnCollisionEnter(Collision other) {
        if (other.gameObject == player) {
            playertouchyterritory = true; 
        }
    }
    private void OnCollisionExit (Collision other) {
        if (other.gameObject == player) {
            playertouchyterritory = false; 
        }
    }

}
