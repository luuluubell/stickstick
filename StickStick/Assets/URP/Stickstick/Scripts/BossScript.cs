﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossScript : MonoBehaviour {
    public Transform targetplayer;
    public float speed = 3f;
    public float attackradius = 1f; //distance that fat man hits you at
    public int damage = 20; 
    public float attackpause; //time between attacks
    
    public void MoveToPlayer () {
        transform.LookAt (targetplayer.position); 
        transform.Rotate (new Vector3 (0, -90, 0), Space.Self); 
            //makes bossman rotate to look at player

        if (Vector3.Distance (transform.position, targetplayer.position) > attackradius) {
            transform.Translate (new Vector3 (speed * Time.deltaTime, 0, 0)); 
        }
            //moves bossman to the player
    }

    public void Rest () { //if player isnt touching the collider then needs to stop
        if (true) {
            
        }
    }
}
